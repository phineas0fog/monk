import os
import pypandoc
import subprocess
import datetime


def parse(app, data):
    date = datetime.datetime.now().isoformat()
    temp_dir = "tmp/{}/".format(date)
    if not os.path.isdir(temp_dir):
        os.makedirs(temp_dir)
    for f in [f for f in os.listdir(temp_dir)]:
        os.remove(temp_dir + '/' + f)

    if 'tocEnable' in data['options']:
        toc = '\\setcounter{tocdepth}{' + data['options']['tocDepth'] + '}\n' \
              '\\tableofcontents\n' \
              '\\newpage\n'
    else:
        toc = ""

    md = data['content']
    la = pypandoc.convert_text(md, "tex", format="md")
    latex = '\\documentclass[' + data['options']['fSize'] + 'pt, french]{article}\n' \
            '\\usepackage[utf8]{inputenc}\n' \
            '\\usepackage{' + data['options']['fFamilly'] + '}\n' \
            '\\usepackage[T1]{fontenc}\n' \
            '\\usepackage{graphicx}\n' \
            '\\usepackage{grffile}\n' \
            '\\usepackage{longtable}\n' \
            '\\usepackage{wrapfig}\n' \
            '\\usepackage{rotating}\n' \
            '\\usepackage[normalem]{ulem}\n' \
            '\\usepackage{amsmath}\n' \
            '\\usepackage{textcomp}\n' \
            '\\usepackage{amssymb}\n' \
            '\\usepackage{capt-of}\n' \
            '\\usepackage{hyperref}\n' \
            '\\usepackage{import}\n' \
            '\\usepackage[a4paper]{geometry}\n' \
            '\\geometry{hscale=0.70,vscale=0.70,centering}\n' \
            '\\usepackage{fancyhdr}\n' \
            '\\usepackage[french]{babel}\n' \
            '\\usepackage{indentfirst}\n' \
            '\\setlength\\parindent{24pt}\n' \
            '\\providecommand{\\tightlist}{%\n' \
            '\\setlength{\\itemsep}{0pt}\\setlength{\\parskip}{0pt}}\n' \
            '\n' \
            '\\begin{document}\n' \
            '\\pagestyle{empty}\n' \
            + toc + \
            '\\pagestyle{plain}\n' \
            '\\pagenumbering{arabic}\n' \
            + la + '\n' \
            '\\end{document}'
    temp_tex_file = temp_dir + "temp.tex"
    f_tex = open(temp_tex_file, "w")
    f_tex.write(latex)
    f_tex.close()
    app.logger.debug("LaTeX file created")
    p = subprocess.Popen(["pdflatex", "-output-directory={}".format(temp_dir), temp_tex_file], stdout=subprocess.PIPE)
    p.wait()
    p = subprocess.Popen(["pdflatex", "-output-directory={}".format(temp_dir), temp_tex_file], stdout=subprocess.PIPE)
    p.wait()
    app.logger.debug("PDF file created")
    return temp_dir + "temp.pdf"
