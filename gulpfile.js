let gulp = require('gulp'),
    compass = require('gulp-compass'),
    concat = require('gulp-concat-css'),
    path = require('path'),
    postcss = require('gulp-postcss'),
    imagemin = require('gulp-imagemin'),
    exec = require('child_process').exec,
    livereload = require('gulp-livereload');

gulp.task('styles', function () {
    let processors = [
        require('postcss-instagram'),
        require('rucksack-css'),
        require('autoprefixer')({browsers: ['last 2 version']}),
        require('css-mqpacker'),
        require('postcss-merge-idents'),
        require('cssnano')({
	    preset: 'advanced',
        })
    ];
    return gulp.src(path.resolve('assets/styles/*.scss'))
        .pipe(compass({
	    css: './assets/css',
	    sass: './assets/styles'
        }))
        .pipe(concat('min.css'))
        .pipe(postcss(processors))
        .pipe(gulp.dest('./public/'))
        .pipe(livereload())
});


gulp.task('imagemin', function () {
    gulp.src('assets/images/*')
		.pipe(imagemin([
            imagemin.optipng({optimizationLevel: 5}),
        ]))
		.pipe(gulp.dest('public/images'))
});

gulp.task('changelog', function () {
    return gulp.src('CHANGELOG.md', {
	buffer: false
    })
	.pipe(conventionalChangelog({
	    preset: 'angular'
	}))
	.pipe(gulp.dest('./'));
});

///////////////////////////////////////////////////////////////////////////////////

gulp.task('watch', function () {
    gulp.watch('assets/styles/*.scss', ['styles']).on('change', function (event) {
        console.log(event.path + ' updated');
    });
});

gulp.task('default', ['styles'], function () {});
