let SimpleMDE = require('simplemde');
import { smoothScroll, getFormData } from "./utils";
import { post } from "./api";
// let $ = require('jquery.min');
import notie from 'notie'

global.displayOptItem = function (id) {
    document.getElementById(id).parentElement.children[1].classList.toggle("hidden");
};

global.editor = function () {
    document.getElementById("content").classList.toggle("hidden");
    document.getElementById("btnContainer").classList.toggle("hidden");
    document.getElementById("editor").classList.toggle("hidden");
};

let simplemde = new SimpleMDE({ element: document.getElementById("editorTextA") });

global.tweak = function () {
    smoothScroll("tweaking");
};

global.render = function () {
    let content = simplemde.value();
    let forms = ["#fFamillyForm", "#fSizeForm", "#tocForm"];
    let options = [];
    let jsonOpt = {};

    if (content === '' || content === undefined) {
        notie.force({
            type: 3,
            text: 'Please type or paste your text',
            buttonText: 'OK',
            callback: function () {
                smoothScroll("editor");
            }
        })
    } else {

        forms.forEach(function (el) {
            options.push(getFormData($(el)));
        });

        for (let i in options) {
            let key = i;
            let val = options[i];
            for (let j in val) {
                let sub_key = j;
                let sub_val = val[j];
                jsonOpt[sub_key] = sub_val;
            }
        }

        let data = {
            "content": content,
            "options": jsonOpt
        };

        post(data);
    }
};