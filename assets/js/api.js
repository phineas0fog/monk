import notie from 'notie'

export function post(data) {
    let http = new XMLHttpRequest();
    let url = "/render";
    let params = JSON.stringify(data);
    http.open("POST", url, true);

    http.setRequestHeader("Content-type", "application/json");

    http.onreadystatechange = function() {
        if(http.readyState === 4 && http.status === 200) {
            console.log(http.responseText);
            let url = JSON.parse(http.responseText).url;
            notie.force({
              type: 1,
              text: 'PDF file created !',
              buttonText: 'OK',
              callback: function () {
                  let win = window.open(url, '_blank');
                  win.focus();
              }
            })
        } else {
            notie.alert({ type: 'info', text: 'Converting...'});
            console.log(http.responseText);
        }
    };

    http.send(params);
}