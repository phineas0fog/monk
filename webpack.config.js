const path = require('path'),
    dev = process.env.NODE_ENV,
    UglifyJSPlugin = require('uglifyjs-webpack-plugin');


module.exports = [{ // CLIENT
    entry: "./assets/js/app.js",
    output: {
        path: path.resolve('./public'),
        filename: 'app.js'
    },
    devtool: dev ? "cheap-module-eval-source-map" : "source-map",
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    },
    resolve: {
        alias: {
            jquery: "assets/js/jquery.min.js"
        }
    },
    plugins: [
    ]
}];