import pypandoc
from pylatex import Document, Section, Subsection, Command
from pylatex.utils import italic, NoEscape
from tex import latex2pdf

with open("temp.md") as fIn:
    md = fIn.read()
    la = pypandoc.convert_text(md, "tex", format="md")
    la = "\\tableofcontents \n" \
         "\\newpage" + la
    fOut = open("out.tex", "w")
    fOut.write(la)
    fOut.close()
    boil = u"\documentclass{article}\n" \
           "\\begin{document}\n" \
           + la + "\n" \
           "\end{document}"
    print(boil)
    pandoc_extrargs = ["--pdf-engine=pdflatex"]
    pypandoc.convert_text(la, to='pdf', format='latex', extra_args=pandoc_extrargs, outputfile='out.pdf')
