from flask import Flask, current_app, request, send_from_directory, render_template, Response
import os
import parser.parser as parser
import shutil
import json
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address


app = Flask(__name__, static_url_path='')
limiter = Limiter(
    app,
    key_func=get_remote_address,
    default_limits=["600/minute"]
)


@app.route('/')
def home():
    return render_template("index.html")


@app.route('/render', methods=['POST'])
@limiter.limit("60/minute")
def render():
    if request.headers['Content-Type'] == 'application/json':
        # app.logger.debug(request.json)
        pdf_file = parser.parse(app, request.json)
        app.logger.debug(pdf_file)
        if os.path.isfile(pdf_file):
            shutil.copy(pdf_file, "./results")

            message = {"text": "PDF file created", "url": pdf_file}
            message = json.dumps(message)

            resp = Response(message, status=200, mimetype='application/json')
        else:
            message = "Error while creating PDF file"
            resp = Response(message, status=500, mimetype='text/plain')
        return resp


@app.route('/public/<path:path>')
def send_resource(path):
    return send_from_directory('public', path)


@app.route('/tmp/<path:path>')
def send_result(path):
    return send_from_directory('tmp', path)


if __name__ == '__main__':
    app.run()
